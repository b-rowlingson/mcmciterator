
###' null progress monitor
###'
###' a progress monitor that does nothing
###' @param mcmcloop an mcmc loop iterator
###' @return a progress monitor
###' @export

mcmcProgressNone <- function(mcmcloop){

  force(mcmcloop)
  
  start = function(){
  }
  update=function(){
  }
  end=function(){
  }

  obj = list(start=start,update=update,end=end)
  class(obj) <- "mcmcprogress"
  return(obj)
  
}

###' printing progress monitor
###'
###' a progress monitor that prints each iteration
###' @param mcmcloop an mcmc loop iterator
###' @return a progress monitor
###' @export

mcmcProgressPrint <- function(mcmcloop){
  force(mcmcloop)

  start = function(){
    cat("Initialising\n")
  }
  update = function(){

    cat("Iteration ",iteration(mcmcloop))
    if(is.burnin(mcmcloop)){
      cat(" - burn-in ")
    }
    if(is.retain(mcmcloop)){
      cat(" - retained")
    }
    cat("\n")
  }
      
  end = function(){
    cat("Finished\n")
  }
  obj = list(start=start,update=update,end=end)
  class(obj) <- "mcmcprogress"
  return(obj)
}

##' Text-based progress bar
##'
##' Uses a progress bar from the progress package
##' @title Text progress bar
##' @param mcmcloop The mcmc iterator
##' @return A progress bar object
##' @author Barry Rowlingson
##' @export
mcmcProgressTextBar <- function(mcmcloop){
  force(mcmcloop)
  pb = progress::progress_bar$new(total=mcmcloop$N,
      format="[:bar] [:i/:N] :percent :state :eta"
      )
  start = function(){
      pb$tick(0)
  }
  update = function(){
      i = iteration(mcmcloop)
      state = ifelse(is.burnin(mcmcloop),"Burn-in",paste0("Sampling [",mcmcloop$samples(),"]"))
      pb$tick(1, tokens=list(state=state, i=i, N=mcmcloop$N))
  }
      
  end = function(){
      #cat("Finished\n")
  }
  obj = list(start=start,update=update,end=end)
  class(obj) <- "mcmcprogress"
  return(obj)
}


###' graphical progress monitor
###'
###' a progress monitor that uses tcltk dialogs
###' @param mcmcloop an mcmc loop iterator
###' @return a progress monitor
###' @export

mcmcProgressTk <- function(mcmcloop){
  force(mcmcloop)
  e=environment()
  start = function(){
    assign("bar",tcltk::tkProgressBar(title="Burn-in",label="Burn-in phase",min=1,max=mcmcloop$N),envir=e)
  }
  update = function(){
    if(is.burnin(mcmcloop)){
      title="Burn-in"
      label=paste("Burn-in ",iteration(mcmcloop),"/",mcmcloop$burnin,sep="")
    }else{
      title="Running"
      label = paste(iteration(mcmcloop),"/",mcmcloop$N,sep="")
      if(is.retain(mcmcloop)){
        label=paste(label," (retained)")
      }
    }
    tcltk::setTkProgressBar(with(e,bar),iteration(mcmcloop),label=label,title=title)
  }
  end = function(){
    close(with(e,bar))
  }
  obj = list(start=start,update=update,end=end)
  class(obj) <- "mcmcprogress"
  return(obj)
}
